using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class About : MonoBehaviour
{
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject aboutMenu;

    private void Awake()
    {
        OnBack();
    }

    public void OnAbout()
    {
        mainMenu.SetActive(false);
        aboutMenu.SetActive(true);
    }

    public void OnBack()
    {
        mainMenu.SetActive(true);
        aboutMenu.SetActive(false);
    }
}
