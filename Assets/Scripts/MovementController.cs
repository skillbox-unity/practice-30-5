using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [SerializeField] private InputHandler inputHandler;
    [SerializeField] private float force;

    private Rigidbody rigidBody;

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        MoveBall();
    }

    private void MoveBall()
    {
        if (inputHandler.IsThereTouchOnScreen())
        {
            Vector2 currDeltaPos = inputHandler.GetTouchDeltaPosition();
            Vector3 currDirectionForce = new Vector3(currDeltaPos.x, 0, currDeltaPos.y);
            rigidBody.AddForce(currDirectionForce * force);
        }
    }
}
