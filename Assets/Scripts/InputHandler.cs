using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    void Update()
    {
        
    }

    public Vector2 GetTouchDeltaPosition()
    {
        if (IsThereTouchOnScreen()) return Input.GetTouch(0).deltaPosition;
        else return Vector2.zero;
    }

    public bool IsThereTouchOnScreen()
    {
        if (Input.touchCount > 0) return true;
        else return false;
    }
}
