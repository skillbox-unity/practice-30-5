using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] private GameObject loseCanvas;
    [SerializeField] private GameObject winCanvas;

    private void Awake()
    {
        loseCanvas.SetActive(false);
        winCanvas.SetActive(false);
    }

    public void ShowLoseScreen()
    {
        loseCanvas.SetActive(true);
    }

    public void ShowWinScreen()
    {
        winCanvas.SetActive(true);
    }
}
