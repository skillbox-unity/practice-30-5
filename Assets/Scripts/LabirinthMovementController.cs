using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabirinthMovementController : MonoBehaviour
{
    [SerializeField] private InputHandler inputHandler;
    [SerializeField] private float force;

    private float angleX = 0.0f;
    private float angleY = 0.0f;
    private float angleZ = 0.0f;

    private Rigidbody rigidBody;

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        
    }

    void FixedUpdate()
    {
        MovePlatform();
        CheckRotation();
    }

    private void MovePlatform()
    {
        if (inputHandler.IsThereTouchOnScreen())
        {
            Vector2 currDeltaPos = inputHandler.GetTouchDeltaPosition();
            Vector3 currDirectionForce = new Vector3(currDeltaPos.x, 0, currDeltaPos.y);
            rigidBody.AddTorque(currDirectionForce * force);
        }
    }

    private void CheckRotation()
    {
        angleX = Mathf.Clamp(Mathf.DeltaAngle(0f, transform.eulerAngles.x), -30.0f, 30.0f);
        angleY = Mathf.Clamp(Mathf.DeltaAngle(0f, transform.eulerAngles.y), -30.0f, 30.0f);
        angleZ = Mathf.Clamp(Mathf.DeltaAngle(0f, transform.eulerAngles.z), -30.0f, 30.0f);
        transform.eulerAngles = new Vector3(angleX, angleY, angleZ);
    }
}
