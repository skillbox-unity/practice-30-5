using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallHitController : MonoBehaviour
{
    private AudioSource hitWall;

    private void Awake()
    {
        hitWall = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            hitWall.Play();
        }
    }
}
