using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    [SerializeField] private Text timerText;
    [SerializeField] private float maxRemainingTime;
    [SerializeField] private UIController uiController;

    private float remainingTime;

    private void Awake()
    {
        remainingTime = maxRemainingTime;
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsRemainingTime())
        {
            remainingTime -= Time.deltaTime;
            DisplayTimer();
        }
        else
        {
            uiController.ShowLoseScreen();
            Time.timeScale = 0;
        }
    }

    public bool IsRemainingTime()
    {
        if (remainingTime > 0) return true;
        else return false;
    }

    private void DisplayTimer()
    {
        if (IsRemainingTime())
        {
            float minutes = Mathf.FloorToInt(remainingTime / 60);
            float seconds = Mathf.FloorToInt(remainingTime % 60);
            timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }
        else
        {
            timerText.text = string.Format("00:00");
        }
    }
}
